## Scope

This repo contains the responses to the tasks mentioned in https://github.com/amitkarpe/devops-test/blob/main/README.md

---

## Status

Status of each tasks are,

1. Kubernetes --> helm chart is completed with wordpress deployment along with a mysql db.
2. Terraform --> Terraform is completed to provision EKS cluster without Load balancer controller.
3. CI/CD --> TODO
4. Build System --> A multistage docker file is completed with sample nodejs application.

---