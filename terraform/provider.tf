provider "aws" {
  region                  = "ap-southeast-1"
  shared_credentials_file = "/Users/maleesh/.aws/credentials"
  profile                 = "futurelabs"
}