locals {
  cluster_name = "futurelabs-eks-cluster"
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "default_vpc" {
  vpc_id = data.aws_vpc.default.id
}

resource "aws_iam_policy" "worker_policy" {
  name        = "worker-policy"
  description = "Worker policy for the ALB Ingress"

  policy = file("iam-policy.json")
}

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  cluster_name = local.cluster_name
  vpc_id       = data.aws_vpc.default.id
  subnet_ids   = data.aws_subnet_ids.default_vpc.ids

  eks_managed_node_groups = {
    blue = {}
    green = {
      min_size     = 1
      max_size     = 2
      desired_size = 1

      instance_types = ["t3.large"]
      capacity_type  = "SPOT"
    }
  }

  cluster_endpoint_private_access = true
  cluster_endpoint_public_access  = true
}

resource "aws_iam_role_policy_attachment" "additional" {
  for_each = module.eks.eks_managed_node_groups
  policy_arn = aws_iam_policy.worker_policy.arn
  role       = each.value.iam_role_name
}